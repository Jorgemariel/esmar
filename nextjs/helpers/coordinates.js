export const getLat = url => {
  // const regex = /(?<=@)(-?[\d\.]*)/; 
  // Lookbehind is not supported in JS
  const regex = /@(-?[\d\.]*)/;
  const lat = regex.exec(url)[0].substr(1);
  return Number(lat);
}

export const getLong = url => {
  // const regex = /(?<=@[-?\d\.]*\,)([-?\d\.]*)/;
  // Lookbehind is not supported in JS
  const regex = /@[-?\d\.]*\,([-?\d\.]*)/;
  const temp = regex.exec(url)[0];
  const regex2 = /,([-?\d\.]*)/;
  const long = regex2.exec(temp)[0].substr(1);
  return Number(long);
}