import { operationHumanizer, typeHumanizer, defaultPropertyType } from 'helpers/defaults';
import _ from 'lodash';

export const filterResolver = string => {
  const splitted = string.split('-en-');
  const types = getTypes(splitted[0]);
  const operation = getOperation(splitted[1]);
  const neighborhoods = operation ? getNeighborhoods(splitted[2]) : getNeighborhoods(splitted[1]);

  return _.pickBy({
    types,
    operation,
    neighborhoods,
  }, _.identity);
}

export const filterStringify = data => {
  let { operation, types, neighborhoods } = data;

  //propiedades-en-alberdi
  //propiedades-en-alquiler
  //propiedades-en-alquilar-en-alberdi

  //departamentos
  //departamentos-en-alberdi
  //departamentos-en-alquiler
  //departamentos-en-alquiler-en-alberdi

  //propiedades-en-nueva-cordoba-y-alberdi
  //propiedades-en-alquiler-en-nueva-cordoba-y-alberdi

  //departamentos-en-nueva-cordoba-y-alberdi
  //departamentos-en-alquiler-en-nueva-cordoba-y-alberdi

  //casas-y-departamentos-en-centro-y-alta-cordoba
  //casas-y-departamentos-en-venta-en-centro-y-alta-cordoba

  types = typesToString(types);
  operation = operationToString(operation);
  neighborhoods = neighborhoodToString(neighborhoods);

  const url = _.filter([types, operation, neighborhoods], _.identity).join('-en-');

  if (url === defaultPropertyType) return null;
  return url;
}

export const urlGenerator = data => {
  const {
    operation,
    types,
    neighborhoods,
    banos,
    cocheras,
    dormitorios,
    precio_max,
    precio_min
  } = data;

  const path = filterStringify({ operation, types, neighborhoods });
  const querystring = generateQuerystring({ banos, cocheras, dormitorios, precio_max, precio_min });

  const url = `/buscar${path ? `/${path}` : ''}${querystring ? `?${querystring}` : ''}`;

  if (url === '/buscar') return '/propiedades';
  return url;
}

const generateQuerystring = input => {
  //precio_max=17000

  const items = Object.keys(input).map(key => input[key] ? encodeURIComponent(key) + '=' + encodeURIComponent(input[key]) : null)

  return _.filter(items, _.identity).join('&');
}

const getTypes = input => {
  if (!input || input === defaultPropertyType) return null;
  const splitted = input.split('-y-');

  const invertedTypeHumanizer = _.invert(typeHumanizer);

  return _.map(splitted, i => invertedTypeHumanizer[i])
}

const typesToString = input => {
  if (!input || input.length === 0) return defaultPropertyType;

  const items = _.map(input, i => typeHumanizer[i]);

  return _.join(items, '-y-')
}

const getOperation = input => {
  const invertedOperationHumanizer = _.invert(operationHumanizer);

  return invertedOperationHumanizer[input] || null;
}

const operationToString = input => {
  console.log({ input })

  return operationHumanizer[input] || null;
}

const getNeighborhoods = input => {
  if (!input || !input.length) return null;
  const splitted = input.toUpperCase().split('-Y-');

  return splitted.map(i => i.replace("-", " "));
}

const neighborhoodToString = input => {
  console.log({ input })
  if (!input || !input.length || !Object.entries(input[0]).length) return null;

  const items = _.map(input, i => i.replace(/\s+/g, '-').toLowerCase());

  return _.join(items, '-y-');
}