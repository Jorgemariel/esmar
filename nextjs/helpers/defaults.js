import path from 'path';

export const coinSimbols = Object.freeze({
  ARS: '$',
  USD: 'U$S',
});

export const logoImage = path.join(__dirname, 'static', 'img', 'logo.png');
export const propertyNoImage = path.join(__dirname, 'static', 'img', 'esmar-default.jpg');
export const defaultPropertyType = 'propiedades';

export const orderTypes = Object.freeze({
  RELEVANTS: 'Más relevantes primeros',
  LOWER_PRICE: 'Menor precio primeros',
  HIGHER_PRICE: 'Mayor precio primeros',
});

export const viewTypes = Object.freeze({
  LIST: 'Listado',
  MAP: 'Mapa',
});

export const operationHumanizer = Object.freeze({
  1: 'alquiler',
  2: 'venta',
});

export const typeHumanizer = Object.freeze({
  DEPARTMENT: 'departamentos',
  HOUSE: 'casas',
  LOCAL: 'locales',
  LOT: 'lotes',
});
