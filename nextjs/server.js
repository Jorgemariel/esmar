const next = require('next')
const routes = require('./routes')
const express = require('express')

const app = next({ dev: process.env.NODE_ENV !== 'production' })
const handler = routes.getRequestHandler(app)
const expressApp = express();

app.prepare().then(() => {
  expressApp.use(handler);
  expressApp.listen(3001);
})