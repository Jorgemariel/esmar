/* eslint-disable */
const webpack = require('webpack');
const withCss = require('@zeit/next-css');

// fix: prevents error when .css files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.css'] = (file) => { };
}

module.exports = withCss({
  distDir: 'build',
  publicRuntimeConfig: {
    COMPANY_EMAIL: process.env.COMPANY_EMAIL || 'jorgemariel.s@gmail.com',
    GOOGLE_MAPS_KEY: process.env.GOOGLE_MAPS_KEY || 'AIzaSyDAPa0hM4bJhHtxDc_NF4UMM-CQqclwvmM',
    GOOGLE_MAPS_ZOOM: process.env.GOOGLE_MAPS_ZOOM || 16,
    NODE_ENV: process.env.NODE_ENV || 'development',
  },
  webpack(config, options) {

    config.plugins.unshift(
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
    );

    return config;
  },
});