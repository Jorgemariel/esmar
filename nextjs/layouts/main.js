import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withRouter } from 'next/router';
import hoistNonReactStatics from 'hoist-non-react-statics';

// Material
import { withStyles } from '@material-ui/core/styles';

// Esmar
import AppBar from 'components/mainLayout/appBar';
import NavBar from 'components/mainLayout/navBar';

const styles = {
  root: {
    height: '100%',
    paddingTop: 56,
    paddingBottom: 56,

    '@media (min-width: 0px) and (orientation: landscape)': {
      paddingTop: 48,
    },
    '@media (min-width: 600px)': {
      paddingTop: 64,
    },
  },
  content: {
    width: '100%',
    height: '100%',

  },
  wrappedComponent: {
    height: '-webkit-fill-available',
  },
};

export default WrappedComponent => {
  const Layout = props => {
    const { classes, ...other } = props;

    return (
      <div className={classNames(classes.root, 'padding-bottom-0-on-keyboard')}>
        <AppBar />
        <div className={classes.content}>
          <WrappedComponent {...other} className={classes.wrappedComponent} />
        </div>
        <div className={'hide-navbar-on-keyboard'}>
          <NavBar />
        </div>
      </div>
    );
  };

  hoistNonReactStatics(Layout, WrappedComponent);

  Layout.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  return withStyles(styles)(withRouter(Layout));
}