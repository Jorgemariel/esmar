import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { withRouter } from 'next/router';
import hoistNonReactStatics from 'hoist-non-react-statics';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    height: '100%',
  }
};

export default WrappedComponent => {
  const Layout = props => {
    const { classes, ...other } = props;

    return (
      <React.Fragment>
        <Head>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.1/css/swiper.min.css" />
          <link rel="stylesheet" href="/static/css/slider.css" />
        </Head>
        <div className={classes.root}>
          <WrappedComponent {...other} className={classes.wrappedComponent} />
        </div>
      </React.Fragment>
    );
  };

  hoistNonReactStatics(Layout, WrappedComponent);

  Layout.propTypes = {
    classes: PropTypes.object.isRequired,
  };

  return withStyles(styles)(withRouter(Layout));
}