import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link';
import { withRouter } from 'next/router';

import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import HomeIcon from '@material-ui/icons/Home';
import SearchIcon from '@material-ui/icons/Search';
import FavoriteIcon from '@material-ui/icons/Favorite';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';

const styles = {
  root: {
    zIndex: 99,
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
};

const LinkWrapper = withRouter(({
  href,
  passHref,
  children,
  router,
  ...props,
}) => (
    <Link href={href} passHref={passHref}>
      {
        React.cloneElement(children, {
          ...props,
          selected: router.pathname === href,
        })
      }
    </Link>
  ));

class NavBar extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <BottomNavigation
        showLabels
        className={classes.root}
      >
        <LinkWrapper passHref href='/'><BottomNavigationAction label="Inicio" icon={<HomeIcon />} /></LinkWrapper>
        <LinkWrapper passHref href='/propiedades'><BottomNavigationAction label="Propiedades" icon={<SearchIcon />} /></LinkWrapper>
        <LinkWrapper passHref href='/contact'><BottomNavigationAction label="Contacto" icon={<MailIcon />} /></LinkWrapper>
      </BottomNavigation>
    );
  }
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);