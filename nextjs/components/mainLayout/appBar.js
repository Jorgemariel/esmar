import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Link from 'next/link'

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

import { logoImage } from 'helpers/defaults';

const styles = theme => ({
  root: {
    zIndex: 99,
    top: 0,
    position: 'absolute',
    width: '100%',
  },
  bar: {
    padding: 0,
  },
  logoWrapper: {
    marginLeft: 16,
    flex: 1,
  },
  logo: {
    width: '8em',
  },
  button: {
    padding: 8,
    marginRight: 8,
    //flex: 1,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

function EsmarAppBar(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar className={classes.bar}>
          <div className={classes.logoWrapper}>
            <Link href="/">
              <img className={classes.logo} src={logoImage} alt="" />
            </Link>
          </div>
          {/*<Button href='tel:3514800200' color="inherit" className={classes.button}>
            <PhoneIcon className={classes.leftIcon} />
            (351) 4 800 200
          </Button>*/}
          <Button color="inherit" className={classes.button}>
            AUTOGESTIÓN
            <AccountCircleIcon className={classes.rightIcon} />
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
}

EsmarAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EsmarAppBar);