import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Router } from '../../routes';

// Material
import { withStyles } from '@material-ui/core/styles';
import Button from 'components/ui/material/button';

// Esmar
import AreaFilter from 'components/index/areaFilter';
import { urlGenerator } from 'helpers/filterResolver';

const styles = {
  root: {
    width: '85%',
    maxWidth: 400,
    margin: '0 auto',
  },
  operationWrapper: {
    display: 'flex',
  },
  button: {
    flex: 1,
    height: 50,
    margin: 5,
    color: 'white',
    border: '1px solid white',
  },
  submitWrapper: {
    textAlign: 'center',
  },
  submit: {
    width: '50%',
    color: 'var(--primary-color)',
    background: 'white',
    marginTop: 10,

    '&:hover': {
      background: 'white',
    }
  },
  buttonActive: {
    color: 'black',
    background: 'white',

    '&:hover': {
      color: 'black',
      background: 'white',
    }
  }
};

const ALQUILER_ID = 1;
const VENTA_ID = 2;

class Search extends React.Component {
  state = {
    operation: 0,
    selectedArea: {},
  };

  setOperation = id => e => {
    if (this.state.operation === id) {
      this.setState({
        operation: 0,
      });
    } else {
      this.setState({
        operation: id,
      });
    }
  };

  handleAreaChange = selectedArea => {
    this.setState({ selectedArea });
  };

  onSearch = () => {
    const { operation, selectedArea } = this.state;
    const url = urlGenerator({
      operation,
      neighborhoods: [selectedArea && selectedArea.label || {}],
    })
    Router.pushRoute(url);
  }

  render() {
    const { areas, classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.operationWrapper}>
          <Button
            variant="outlined"
            onClick={this.setOperation(ALQUILER_ID)}
            className={classNames(classes.button, { [classes.buttonActive]: this.state.operation === ALQUILER_ID })}
          >
            ALQUILER
          </Button>
          <Button
            variant="outlined"
            onClick={this.setOperation(VENTA_ID)}
            className={classNames(classes.button, { [classes.buttonActive]: this.state.operation === VENTA_ID })}
          >
            VENTA
          </Button>
        </div>
        <div>
          <AreaFilter areas={areas} onSelect={this.handleAreaChange} />
        </div>
        <div className={classes.submitWrapper}>
          <Button variant="outlined" className={classNames(classes.button, classes.submit)} onClick={this.onSearch}>
            BUSCAR
          </Button>
        </div>
      </div>
    );
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Search);