import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import CustomFab from 'components/ui/material/fab';
import PhoneIcon from '@material-ui/icons/Phone';
import { FaQuestion } from 'react-icons/fa';

const styles = theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  call: {
    color: 'var(--primary-color)',
    marginRight: 'auto',
    background: 'white',
  },
  help: {
    color: 'white',
    fontSize: '1em',
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  },
});

const FloatingAction = ({ classes }) => {
  return (
    <div className={classes.root}>
      <CustomFab href={`tel:${3514800200}`} variant="extended" aria-label="Telefono" className={classes.call}>
        <PhoneIcon className={classes.extendedIcon} />
        (351) 4 800 200
      </CustomFab>
      <Fab variant="extended" color="secondary" aria-label="Ayuda" className={classes.help}>
        <FaQuestion />
      </Fab>
    </div>
  );
};

FloatingAction.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FloatingAction);
