import React from 'react';
import Downshift from 'downshift';
import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

const styles = {
  area: {
    width: '-webkit-fill-available',
    marginTop: 5,
  },
  constainer: {

  },
  inputRoot: {
    margin: 5,
    color: 'white',

    '&::placeholder': {
      color: 'white',
    }
  },
  inputInput: {
    padding: '15.5px 14px',
    fontSize: '0.875rem',
    fontWeight: 500,
    letterSpacing: '0.02857em',
    textAlign: 'center',

    '&::placeholder': {
      opacity: '1 !important',
      color: 'white'
    }
  },
  border: {
    border: 'solid white 1px',
    borderColor: 'white !important',
    borderRadius: 'var(--border-radius)',

    '&:hover': {
      border: 'solid white 1px',
      backgroundColor: 'rgba(0, 0, 0, 0.08)',
    }
  },
  paper: {
    transition: 2,
    width: '90%',
    margin: '0 auto',
    marginTop: '-5px',
    borderRadius: '0 0 4px 4px',
  }
};

class AreaFilter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      area: '',
      placeholder: 'ZONA O BARRIO',
      suggestions: props.areas,
    }
  }
  renderInput(inputProps) {
    const { InputProps, classes, ref, ...other } = inputProps;

    //console.log({ InputProps, classes, ref, ...other })

    return (
      <TextField
        className={classes.area}
        InputProps={{
          inputRef: ref,
          classes: {
            root: classes.inputRoot,
            input: classes.inputInput,
            notchedOutline: classes.border,
          },
          ...InputProps,
        }}
        {...other}
      />
    );
  };

  getSuggestions(value) {
    const inputValue = deburr(value.trim()).toLowerCase();
    const inputLength = inputValue.length;
    let count = 0;

    return inputLength === 0
      ? []
      : this.state.suggestions.filter(suggestion => {
        const keep =
          count < 4 && suggestion.label.toLowerCase().includes(inputValue);

        if (keep) {
          count += 1;
        }

        //console.log({ value, keep })

        return keep;
      });
  };

  renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem }) {
    const isHighlighted = highlightedIndex === index;
    const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;

    //console.log({ suggestion, index, itemProps, highlightedIndex, selectedItem })

    return (
      <MenuItem
        {...itemProps}
        key={suggestion.id}
        selected={isHighlighted}
        component="div"
        style={{
          fontWeight: isSelected ? 500 : 400,
        }}
      >
        {suggestion.label}
      </MenuItem>
    );
  };

  render() {
    const { onSelect, classes } = this.props;

    return (
      <Downshift
        id="area"
        onChange={onSelect}
        itemToString={item => item ? item.label : ''}
      >
        {({
          getInputProps,
          getItemProps,
          getMenuProps,
          highlightedIndex,
          inputValue,
          isOpen,
          selectedItem,
        }) => (
            <div className={classes.container}>
              {this.renderInput({
                variant: "outlined",
                classes,
                InputProps: getInputProps({
                  placeholder: this.state.placeholder,
                }),
              })}
              <div {...getMenuProps()}>
                {isOpen ? (
                  <Paper className={classes.paper} square>
                    {this.getSuggestions(inputValue).map((suggestion, index) =>
                      this.renderSuggestion({
                        suggestion,
                        index,
                        itemProps: getItemProps({ item: suggestion }),
                        highlightedIndex,
                        selectedItem,
                      }),
                    )}
                  </Paper>
                ) : null}
              </div>
            </div>
          )}
      </Downshift>
    )
  }
}

AreaFilter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AreaFilter);