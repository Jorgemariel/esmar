import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Divider from '@material-ui/core/Divider';
import FormGroup from '@material-ui/core/FormGroup';

import Dialog from 'components/ui/material/dialog';
import Button from 'components/ui/material/button';
import AppliedFilters from 'components/search/appliedFilters';
import { capitalizeFirstLetter } from 'helpers/capitalize';
import FilterItem from 'components/search/filterItem';
import { operationHumanizer, typeHumanizer } from 'helpers/defaults';
import CustomCheckbox from 'components/ui/customCheckbox';

const styles = {
  checkList: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  check: {
    flex: 1,
  },
};

class ModalFilter extends React.Component {
  state = {
    operation: false,
    types: false,
    showNeighborhoods: false,
  }

  toggle(item) {
    this.setState({ [item]: !this.state[item] });
  }

  render() {
    const { open, filters, neighborhoods, onClose, onChange, onDelete, classes } = this.props;
    const { operation, types, showNeighborhoods } = this.state;

    return (
      <Dialog
        open={open || false}
        fullWidth={true}
        onClose={onClose}
        aria-labelledby="form-dialog-title"
      >
        <AppliedFilters items={filters} onDelete={onDelete} neighborhoodsList={neighborhoods} />
        <DialogTitle id="form-dialog-title">Filtros</DialogTitle>

        <FilterItem title='Operacion' open={operation} onClick={() => this.toggle('operation')}>
          <RadioGroup
            aria-label="Operacion"
            name="operation"
            value={filters.operation}
            onChange={(item) => onChange('operation', item.target.value)}
          >
            {Object.keys(operationHumanizer).map(key =>
              <FormControlLabel key={key} value={key} control={<Radio />} label={capitalizeFirstLetter(operationHumanizer[key])} />
            )}
          </RadioGroup>
        </FilterItem>

        <Divider />

        <FilterItem title='Tipo de inmueble' open={types} onClick={() => this.toggle('types')}>
          <div className={classes.checkList}>
            {Object.keys(typeHumanizer).map(key =>
              <FormControlLabel key={key}
                control={
                  <Checkbox checked={!!filters.types && filters.types.includes(key)} value={key} onChange={(item) => onChange('types', item.target.value)} />
                }
                label={capitalizeFirstLetter(typeHumanizer[key])}
                classes={{ root: classes.check }}
              />
            )}
          </div>
        </FilterItem>

        <Divider />

        <FilterItem
          title='Barrio o Zona'
          open={showNeighborhoods}
          timeout={400}
          onClick={() => this.toggle('showNeighborhoods')}
        >
          <FormGroup>
            {neighborhoods.map(item =>
              <CustomCheckbox
                key={item.id}
                label={capitalizeFirstLetter(item.label)}
                name={item.label}
                checked={filters.neighborhoods && filters.neighborhoods.includes(item.id)}
                onChange={() => onChange('neighborhoods', item.id)}
              />
            )}
          </FormGroup>
        </FilterItem>

        <DialogActions>
          <Button onClick={onClose} color="primary">
            Aplicar
        </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

ModalFilter.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ModalFilter);