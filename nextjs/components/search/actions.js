import React from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';

import IconButton from '@material-ui/core/IconButton';
import Favorite from '@material-ui/icons/Favorite';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ShareIcon from '@material-ui/icons/Share';

const styles = {
  root: {
    color: 'white',
  },
  fav: {
    margin: '0 !important',
  },
  favIcon: {
    color: 'white',
  },
  share: {
    color: 'white',
  },
};

const Actions = ({ fav, onFav, onShare, classes }) => {
  return (
    <div className={classes.root}>
      <FormControlLabel className={classes.fav}
        control={
          <Checkbox
            className={classes.favIcon}
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite />}
            value="checkedH"
            onChange={onFav}
            checked={fav}
          />
        }
      />
      {/* <IconButton
        aria-label="Share"
        className={classes.share}
        onClick={onShare}
      >
        <ShareIcon />
      </IconButton> */}
    </div>
  )
}

Actions.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Actions);