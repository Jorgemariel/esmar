import React from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';
import _ from 'lodash';

import { withStyles } from '@material-ui/core/styles';
import Chip from 'components/ui/material/chip';
import CloseIcon from '@material-ui/icons/Close';
import { operationHumanizer, typeHumanizer } from 'helpers/defaults';
import { capitalizeFirstLetter } from 'helpers/capitalize';

const styles = theme => ({
  root: {
    width: '100%',
    background: 'white',
    //padding: 5,
    display: 'flex',
    flexFlow: 'row wrap',
    overflow: 'auto',
  },
  padding5: {
    padding: 5,
  },
  chip: {
    margin: 5,
    color: 'var(--grey-color)',
    backgroundColor: 'var(--lightgrey-color)',
    padding: '20px 0',

    '&:focus': {
      backgroundColor: 'var(--lightgrey-color)',
    }
  },
  closeIcon: {
    fontSize: 16,
    marginLeft: -6,
    marginRight: 5,
  }
});

const AppliedFilters = ({ classes, items, neighborhoodsList, onDelete }) => {
  const {
    operation,
    types,
    neighborhoods,
    dormitorios,
    precio_max,
    precio_min,
    cocheras,
    banos,
  } = items;

  const nhList = _.keyBy(neighborhoodsList, 'id');

  return (
    <div className={classNames(classes.root, { [classes.padding5]: items.length > 0 })}>
      {!!operation &&
        <Chip
          label={capitalizeFirstLetter(operationHumanizer[operation])}
          onDelete={() => onDelete('operation')}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      }
      {!!types && types.map(type =>
        <Chip
          key={type}
          label={capitalizeFirstLetter(typeHumanizer[type])}
          onDelete={() => onDelete('types', type)}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      )}
      {!!neighborhoods && neighborhoods.map(nh =>
        <Chip
          key={nh}
          label={capitalizeFirstLetter(nhList[nh].label)}
          onDelete={() => onDelete('neighborhoods', nh)}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      )}
      {!!dormitorios &&
        <Chip
          label={`${dormitorios}+ ${dormitorios === 1 ? 'Dormitorio' : 'Dormitorios'}`}
          onDelete={() => onDelete('dormitorios')}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      }
      {!!banos &&
        <Chip
          label={`${banos}+ ${banos === 1 ? 'Bano' : 'Banos'}`}
          onDelete={() => onDelete('banos')}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      }
      {!!cocheras &&
        <Chip
          label={`${cocheras != 1 ? cocheras : ''} ${cocheras == 1 ? 'Cochera' : 'Cocheras'}`}
          onDelete={() => onDelete('cocheras')}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      }
      {!!precio_min &&
        <Chip
          label={`Min: $${precio_min}`}
          onDelete={() => onDelete('precio_min')}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      }
      {!!precio_max &&
        <Chip
          label={`Max: $${precio_max}`}
          onDelete={() => onDelete('precio_max')}
          deleteIcon={<CloseIcon className={classes.closeIcon} />}
          className={classes.chip}
        />
      }
    </div>
  )
}

AppliedFilters.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppliedFilters);