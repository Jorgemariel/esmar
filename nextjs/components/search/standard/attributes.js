import React from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import { FaBed, FaBath } from 'react-icons/fa';

import Tooltip from '@material-ui/core/Tooltip';

import GarageIcon from 'components/ui/garageIcon';


const styles = {
  root: {
    display: 'flex',
    background: 'var(--lightgrey-color)',
    textAlign: 'center',
    width: '100%',
    overflow: 'auto',
    alignItems: 'center',
    height: 50,
    padding: '0 5px',
  },
  footerItem: {
    padding: '12px 10px',
    flex: 1,
    minWidth: 55,
  },
  icon: {
    verticalAlign: 'bottom',
  },
  garageIcon: {
    width: '1.45em !important',
    fill: 'var(--grey-color)',
  },
  tooltip: {
    marginTop: -10,
  },
}

const Attributes = ({ bathrooms, bedrooms, garage, classes }) => {
  return (
    <div className={classes.root}>
      {!!bedrooms &&
        <Tooltip title={`${bedrooms} dormitorio/s`} placement="bottom" classes={{ tooltip: classes.tooltip }}>
          <div className={classes.footerItem} >{bedrooms} <FaBed size='1.15em' className={classes.icon} /></div>
        </Tooltip>
      }
      {!!bathrooms &&
        <Tooltip title={`${bathrooms} baño/s`} placement="bottom" classes={{ tooltip: classes.tooltip }}>
          <div className={classes.footerItem} >{bathrooms} <FaBath size='1.15em' className={classes.icon} /></div>
        </Tooltip>
      }
      {!!garage &&
        <Tooltip title={'Tiene cochera'} placement="bottom" classes={{ tooltip: classes.tooltip }}>
          <div className={classes.footerItem} ><GarageIcon size='1.15em' className={classNames(classes.icon, classes.garageIcon)} /></div>
        </Tooltip>
      }
    </div>
  );
};

Attributes.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(Attributes);