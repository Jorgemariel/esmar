import React from 'react'
import PropTypes from 'prop-types';
import { FaMapMarkerAlt } from 'react-icons/fa';

// Material
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    fontSize: '3em',
    color: 'var(--primary-color)',
    position: 'absolute',
    transform: 'translate(-50%, -50%)',
  },
};

class PropertyPinView extends React.Component {
  state = {
    isOpen: false,
  }

  handleState = (key, value) => {
    this.setState({ [key]: value });
  }

  render() {
    const { classes } = this.props;
    const { isOpen } = this.state;

    return (
      <div className={classes.root}>
        {!isOpen ?
          <FaMapMarkerAlt onClick={() => this.handleState('isOpen', true)} />
          :
          <div onClick={() => this.handleState('isOpen', false)}>
            hola
          </div>
        }
      </div>
    )
  }
}

PropertyPinView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PropertyPinView);