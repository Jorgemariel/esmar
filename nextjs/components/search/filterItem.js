import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import Typography from '@material-ui/core/Typography';
import DownIcon from '@material-ui/icons/KeyboardArrowDown';

const styles = {
  root: {
    color: 'var(--grey-color)',
  },
  header: {
    padding: 10,
    display: 'flex',
    alignItems: 'center',
  },
  title: {
    flex: 1,
  },
  open: {
    transform: 'rotate(180deg)',
    transition: 'all 200ms linear',
  },
  close: {
    transition: 'all 200ms linear',
  },
  body: {
    maxHeight: 300,
    overflow: 'auto',
    padding: 10,
    paddingTop: 0,
  }
};

const FilterItem = ({ title, open, timeout, children, onClick, classes }) => {
  return (
    <div className={classes.root}>
      <div onClick={onClick} className={classes.header}>
        <div className={classes.title}><Typography variant="body1">{title}</Typography></div>
        <DownIcon className={open ? classes.open : classes.close} />
      </div>
      <Collapse className={classes.body} in={open} timeout={timeout || 'auto'} unmountOnExit>
        {children}
      </Collapse>
    </div>
  )
}

FilterItem.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FilterItem);
