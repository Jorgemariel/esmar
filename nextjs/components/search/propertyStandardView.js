import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from '../../routes';

import Card from 'components/ui/material/card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

import Attributes from 'components/search/standard/attributes';
import { coinSimbols, propertyNoImage } from 'helpers/defaults';


const styles = theme => ({
  root: {
    width: '100%',
  },
  card: {
    width: '100%',
  },
  media: {
    height: 225,
    width: '100%',
    position: 'relative',
    boxShadow: 'inset 0px -60px 60px -15px rgba(0,0,0,0.5)',
  },
  price: {
    position: 'absolute',
    bottom: 3,
    left: 15,
    color: 'white',
  },
  operation: {
    position: 'absolute',
    bottom: 3,
    right: 15,
    color: 'white',
    textTransform: 'uppercase',
    border: 'solid 1px',
    padding: '5px 15px',
    borderRadius: 'var(--border-radius)',
  },
  name: {
    fontSize: '1.1rem',
  },
  neighborhood: {
    fontSize: '0.85rem',
  },
  footer: {
    color: 'var(--grey-color)',
    padding: 0,
  },
  left: {
    flex: 1,
    margin: 0,
    marginLeft: 4,
    maxWidth: 230,
    overflow: 'auto',
    borderRadius: 'var(--border-radius)',
  },
});

const PropertyStandardView = ({ classes, data, onFav, onShare }) => (
  <Card className={classes.root} elevation={2}>
    <CardActionArea>
      <CardMedia
        className={classes.media}
        image={data.images[0] || propertyNoImage}
        title={data.name}
      >
        {!!data.price &&
          <Typography gutterBottom variant="h5" className={classes.price}>
            {coinSimbols[data.coin]}{data.price}
          </Typography>
        }
        <Typography gutterBottom variant="subtitle1" className={classes.operation}>
          {data.operation}
        </Typography>
      </CardMedia>
      <CardContent>
        <Typography gutterBottom variant="h5" className={classes.name}>
          {data.name}
        </Typography>
        <Typography component="p" className={classes.neighborhood}>
          {data.neighborhood}
        </Typography>
      </CardContent>
      <CardActions className={classes.footer}>
        <Attributes {...data} />
      </CardActions>
    </CardActionArea>
  </Card>
);

PropertyStandardView.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PropertyStandardView);