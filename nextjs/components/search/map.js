import React from 'react';
import GoogleMapReact from 'google-map-react';
import getConfig from 'next/config';

//ENV
const { publicRuntimeConfig: { GOOGLE_MAPS_KEY, GOOGLE_MAPS_ZOOM } } = getConfig();

class Map extends React.Component {
  render() {
    const { children } = this.props;

    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '630px', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
          defaultCenter={{
            lat: -31.4107688,
            lng: -64.1957173,
          }}
          defaultZoom={GOOGLE_MAPS_ZOOM - 4.5}
        >
          {children}
        </GoogleMapReact>
      </div>
    );
  }
}

export default Map;