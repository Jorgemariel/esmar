import React from 'react'
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FilterListIcon from '@material-ui/icons/FilterList';
import OrderIcon from '@material-ui/icons/Reorder';
import ListIcon from '@material-ui/icons/ViewModule';
import MapIcon from '@material-ui/icons/Map';
import { viewTypes } from 'helpers/defaults';


const styles = theme => ({
  root: {
    width: '100%',
    display: 'flex',
    padding: '10px 0',
    background: 'white',
  },
  button: {
    flex: 1,
    padding: 8,
    fontWeight: 200,
    borderRadius: 0,
    color: '#3483fa',
  },
  middleButton: {
    borderLeft: 'solid 1px #DDD',
    borderRight: 'solid 1px #DDD',
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

const Options = ({ classes, selectedView, openOrder, openFilter, toggleView, orderDialog }) => {
  return (
    <div className={classes.root}>
      <Button
        color="inherit"
        className={classes.button}
        onClick={openOrder}
        aria-owns={Boolean(orderDialog) ? 'simple-menu' : null}
        aria-haspopup="true"
      >
        <OrderIcon className={classes.leftIcon} />
        Ordenar
      </Button>
      <Button
        color="inherit"
        className={classNames(classes.button, classes.middleButton)}
        onClick={openFilter}
      >
        <FilterListIcon className={classes.leftIcon} />
        Filtrar
      </Button>
      <Button
        color="inherit"
        className={classes.button}
        onClick={toggleView}
      >
        {selectedView === viewTypes.LIST ?
          <React.Fragment>
            <MapIcon className={classes.leftIcon} />
            <span>Ver Mapa</span>
          </React.Fragment>
          :
          <React.Fragment>
            <ListIcon className={classes.leftIcon} />
            <span>Ver Lista</span>
          </React.Fragment>
        }
      </Button>
    </div>
  )
}

Options.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Options);