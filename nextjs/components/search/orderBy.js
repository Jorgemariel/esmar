import React from 'react'
import PropTypes from 'prop-types';

import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import { orderTypes } from 'helpers/defaults';

const styles = {
  root: {
    marginTop: 70,
  },
  item: {
    width: '100vw',
  },
}

const Orders = ({ classes, orderDialog, onChangeOrder, onClose, selectedOrder }) => {
  return (
    <div>
      <Menu
        className={classes.root}
        menustyle={{ width: 'auto', backgroundColor: 'red' }}
        id="simple-menu"
        anchorEl={orderDialog}
        open={Boolean(orderDialog)}
        onClose={onClose}
      >
        {Object.keys(orderTypes).map(o => (
          <MenuItem key={o} selected={o === selectedOrder} className={classes.item} onClick={() => onChangeOrder(o)}>{orderTypes[o]}</MenuItem>
        ))}
      </Menu>
    </div>
  )
}

Orders.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Orders);