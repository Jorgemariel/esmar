import React from 'react';
import PropTypes from 'prop-types';
import Link from 'next/link'

// Material
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import LeftIcon from '@material-ui/icons/Share';

// Custom
import Actions from 'components/search/actions';
import { logoImage } from 'helpers/defaults';

const styles = {
  root: {
    padding: '5px 5px',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: 'white',
    background: 'var(--primary-color)',
  },
  back: {
    width: 48,
  },
  logoWrapper: {
    //marginLeft: 16,
    flex: 1,
    textAlign: 'center',
  },
  logo: {
    width: '8em',
    verticalAlign: 'middle',
  },
  fav: {
    width: 48,
  }
};

const Header = ({ fav, onShare, onFav, classes }) => (
  <div className={classes.root}>
    <div className={classes.back}>
      <IconButton
        key="share"
        aria-label="Share"
        color="inherit"
        onClick={onShare}
      >
        <LeftIcon />
      </IconButton>
    </div>
    <div className={classes.logoWrapper}>
      <Link href="/">
        <img className={classes.logo} src={logoImage} alt="" />
      </Link>
    </div>
    <div className={classes.back}><Actions fav={fav} onFav={onFav} onShare={onShare} /></div>
  </div>
);

Header.propTypes = {
  fav: PropTypes.bool.isRequired,
  onFav: PropTypes.func.isRequired,
  onShare: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);