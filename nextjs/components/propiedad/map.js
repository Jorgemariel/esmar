import React from 'react';
import GoogleMapReact from 'google-map-react';
import getConfig from 'next/config';

//ENV
const { publicRuntimeConfig: { GOOGLE_MAPS_KEY, GOOGLE_MAPS_ZOOM } } = getConfig();

// Custom
import Pin from 'components/ui/mapPin';
import { getLat, getLong } from 'helpers/coordinates';



class Map extends React.Component {
  render() {
    const { google_maps } = this.props;

    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '500px', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: GOOGLE_MAPS_KEY }}
          defaultCenter={{
            lat: getLat(google_maps),
            lng: getLong(google_maps),
          }}
          defaultZoom={GOOGLE_MAPS_ZOOM}
        >
          <Pin
            lat={getLat(google_maps)}
            lng={getLong(google_maps)}
            text={'Pin'}
          />
        </GoogleMapReact>
      </div>
    );
  }
}

export default Map;