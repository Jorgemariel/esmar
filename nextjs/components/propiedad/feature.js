import React from 'react';
import PropTypes from 'prop-types';

// Material
import { withStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const styles = {
  root: {
    justifyContent: 'space-between',
    display: 'flex',
  },
  left: {
    display: 'flex',
    alignItems: 'center',
  },
  right: {
    color: 'var(--grey-color)',
    width: 50,
    textAlign: 'right',
    maxHeight: 25,
    marginRight: 10,
  },
  icon: {
    color: 'var(--grey-color)',
    fill: 'var(--grey-color)',
    marginRight: 10,
    fontSize: 20,
    width: 25,
    textAlign: 'center',
  }
}

const Feature = ({ name, text, icon, classes }) => (
  <div className={classes.root}>
    <div className={classes.left}><div className={classes.icon}>{icon}</div> <Typography variant="body2">{name}</Typography></div>
    <div className={classes.right}><Typography variant="body2">{text}</Typography></div>
  </div>
);

Feature.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Feature)