import React from 'react';
import PropTypes from 'prop-types';
import { FaSwimmingPool, FaArrowAltCircleUp, FaDumbbell, FaSpa } from 'react-icons/fa';

// Material
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from 'components/ui/material/paper';
import SecurityIcon from '@material-ui/icons/Security';
import CheckIcon from '@material-ui/icons/Check';

// Custom
import Feature from 'components/propiedad/feature';

const styles = {
  root: {
    padding: '15px 20px',
    width: '100%',
  },
  wrapper: {
    maxWidth: 400,
    margin: '0 auto',
  },
  feature: {
    padding: '15px 10px',

    '&:nth-child(even)': {
      background: 'var(--lightgrey-color)',
    }
  },
};

const Amenities = ({ amenities = {}, classes }) => (
  <div className={classes.root}>
    <Typography variant="h6" gutterBottom>Amenities</Typography>
    <Paper elevation={1} className={classes.wrapper}>
      {!!amenities.private_security &&
        <div className={classes.feature}><Feature name='Seguridad Privada' icon={<SecurityIcon />} text={<CheckIcon />} /></div>
      }
      {!!amenities.swimming_pool &&
        <div className={classes.feature}><Feature name='Pileta' icon={<FaSwimmingPool />} text={<CheckIcon />} /></div>
      }
      {!!amenities.gym &&
        <div className={classes.feature}><Feature name='Gimnasio' icon={<FaDumbbell />} text={<CheckIcon />} /></div>
      }
      {!!amenities.gym &&
        <div className={classes.feature}><Feature name='Ascensores' icon={<FaArrowAltCircleUp />} text={<CheckIcon />} /></div>
      }
      {!!amenities.gym &&
        <div className={classes.feature}><Feature name='Spa' icon={<FaSpa />} text={<CheckIcon />} /></div>
      }
    </Paper>
  </div>
);

Amenities.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Amenities)