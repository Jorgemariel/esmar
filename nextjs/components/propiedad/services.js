import React from 'react';
import PropTypes from 'prop-types';
import { FaLightbulb, FaTint, FaUndo, FaBurn, FaRoad } from 'react-icons/fa';

// Material
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from 'components/ui/material/paper';
import CheckIcon from '@material-ui/icons/Check';

// Custom
import Feature from 'components/propiedad/feature';

const styles = {
  root: {
    padding: '15px 20px',
    width: '100%',
  },
  wrapper: {
    maxWidth: 400,
    margin: '0 auto',
  },
  feature: {
    padding: '15px 10px',

    '&:nth-child(even)': {
      background: 'var(--lightgrey-color)',
    }
  },
};

const Services = ({ services = {}, classes }) => (
  <div className={classes.root}>
    <Typography variant="h6" gutterBottom>Servicios</Typography>
    <Paper elevation={1} className={classes.wrapper}>
      {!!services.light &&
        <div className={classes.feature}><Feature name='Luz' icon={<FaLightbulb />} text={<CheckIcon />} /></div>
      }
      {!!services.water &&
        <div className={classes.feature}><Feature name='Agua' icon={<FaTint />} text={<CheckIcon />} /></div>
      }
      {!!services.gas &&
        <div className={classes.feature}><Feature name='Gas' icon={<FaBurn />} text={<CheckIcon />} /></div>
      }
      {!!services.gas &&
        <div className={classes.feature}><Feature name='Pavimento' icon={<FaRoad />} text={<CheckIcon />} /></div>
      }
      {!!services.gas &&
        <div className={classes.feature}><Feature name='Desague Cloacal' icon={<FaUndo />} text={<CheckIcon />} /></div>
      }
    </Paper>
  </div>
);

Services.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Services)