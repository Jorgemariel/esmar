import React from 'react';
import PropTypes from 'prop-types';

// Material
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ShareIcon from '@material-ui/icons/Share';
import ChatIcon from '@material-ui/icons/Chat';


const styles = {
  root: {
    textAlign: 'center',
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0,
    background: 'var(--primary-color)',
    color: 'white',
    zIndex: 99,
  },
  consultar: {
    cursor: 'pointer',
    margin: '0 auto',
    color: 'white',
    padding: 20,
    width: '100%',
  },
  leftIcon: {
    marginRight: 15,
  }
};

const Actions = ({ classes, onClickContact }) => (
  <div className={classes.root}>
    <Button className={classes.consultar} style={{ cursor: 'pointer' }} size="large" onClick={() => onClickContact(true)}>
      <ChatIcon className={classes.leftIcon} /> Contactar
    </Button>
  </div >
);

Actions.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Actions)