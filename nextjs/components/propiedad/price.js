import React from 'react';
import PropTypes from 'prop-types';

// Material
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import LeftIcon from '@material-ui/icons/ChevronLeft';
import Typography from '@material-ui/core/Typography';

// Custom
import Actions from 'components/search/actions';

const styles = {
  root: {
    padding: 20,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    color: 'white',
    background: 'var(--secondary-color)',
  },
  left: {
    color: 'white',
  },
  right: {
    color: 'white',
    fontWeight: 'bold',
  }
};

const Price = ({ type, coin, price, operation, classes }) => (
  <div className={classes.root}>
    <div className={classes.left}>
      <Typography variant="subtitle1" color='inherit'>{type} en {operation}</Typography>
    </div>
    <div className={classes.right}>
      <Typography variant="subtitle1" color='inherit'>{coin}{price}</Typography>
    </div>

  </div>
);

Price.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Price)