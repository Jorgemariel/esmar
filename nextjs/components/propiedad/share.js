import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { CopyToClipboard } from 'react-copy-to-clipboard';

// Icons
import { FaWhatsapp, FaTwitter, FaFacebook } from 'react-icons/fa';
import EmailIcon from '@material-ui/icons/Email';
import CopyIcon from '@material-ui/icons/FileCopy';


//Material
import { withStyles } from '@material-ui/core/styles';
import Button from 'components/ui/material/button';
import Dialog from 'components/ui/material/dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { capitalizeFirstLetter } from 'helpers/capitalize';

const styles = {
  dialog: {
    margin: 10,
  },
  dialogContent: {
    padding: '0 !important',
  },
  shareButton: {
    margin: 20,
    padding: '20px 15px',
    width: '-webkit-fill-available',
    //width: '-moz-available',
    display: 'flex',
  },
  shareButtonIcon: {
    width: 30,
    fontSize: 'x-large',
  },
  shareButtonText: {
    flex: 1,
    paddingLeft: 15,
    textAlign: 'left',
  },
  copy: {
    backgroundColor: '#1383D8',
    color: 'white',
  },
  whatsapp: {
    backgroundColor: '#65B256',
    color: 'white',
  },
  twitter: {
    backgroundColor: '#1da1f2',
    color: 'white',
  },
  email: {
    backgroundColor: '#E04F4C',
    color: 'white',
  },
  facebook: {
    backgroundColor: '#3b5998',
    color: 'white',
  }
}

class Share extends React.Component {
  state = {
    email: 'shareo@esmar.com.ar',
    emailSubject: encodeURIComponent('Consulta desde pagina web'),
    phone: 3514800200,
    phoneToString: '(351) 4 800 200',
    whatsappPhone: 5493515144316,
    url: '',
  }

  componentDidMount() {
    const { name, type, operation, neighborhood } = this.props.data;
    const message = `*${capitalizeFirstLetter(name)}* \n${type} en ${operation} en ${capitalizeFirstLetter(neighborhood)} \n\n${document.location.href} \n\nGrupo Esmar - Valores Inmuebles`;
    const message2 = `${capitalizeFirstLetter(name)} \n${type} en ${operation} en ${capitalizeFirstLetter(neighborhood)} \n\n${document.location.href} \n\nGrupo Esmar - Valores Inmuebles \n`;

    if (typeof document !== 'undefined') {
      this.setState({
        message: encodeURIComponent(message),
        message2: encodeURIComponent(message2),
        url: document.location.href,
      });
    }
  }
  render() {
    const { shareDialog, handleShareDialog, handleCopied, classes } = this.props;
    const { message, message2, whatsappPhone, email, emailSubject, url } = this.state;

    return (
      <Dialog
        open={shareDialog}
        onClose={() => handleShareDialog(false)}
        PaperProps={{ classes: { root: classes.dialog } }}
      >
        <DialogContent className={classes.dialogContent}>
          <Button
            size="large"
            className={classNames(classes.shareButton, classes.whatsapp)}
            href={`https://wa.me?text=${message}`}
            onClick={() => handleShareDialog(false)}
          >
            <FaWhatsapp className={classes.shareButtonIcon} />
            <span className={classes.shareButtonText}>Enviar por Whatsapp</span>
          </Button>
          <CopyToClipboard
            text={url}
            onCopy={handleCopied}
          >
            <Button
              size="large"
              className={classNames(classes.shareButton, classes.copy)}
              onClick={() => handleShareDialog(false)}
            >
              <CopyIcon className={classes.shareButtonIcon} />
              <span className={classes.shareButtonText}>Copiar Link</span>
            </Button>
          </CopyToClipboard>
          <Button
            size="large"
            className={classNames(classes.shareButton, classes.facebook)}
            href={`https://www.facebook.com/sharer/sharer.php?u=${url}&quote=${message2}`}
            target='_blank'
            onClick={() => handleShareDialog(false)}
          >
            <FaFacebook className={classes.shareButtonIcon} />
            <span className={classes.shareButtonText}>Compartir en Facebook</span>
          </Button>
          <Button
            size="large"
            className={classNames(classes.shareButton, classes.twitter)}
            href={`https://twitter.com/intent/tweet?via=grupoesmar&text=${message2}`}
            target='_blank'
            onClick={() => handleShareDialog(false)}
          >
            <FaTwitter className={classes.shareButtonIcon} />
            <span className={classes.shareButtonText}>Twittear</span>
          </Button>
          <Button
            size="large"
            className={classNames(classes.shareButton, classes.email)}
            href={`mailto:${email}?subject=${emailSubject}&body=${message}`}
            onClick={() => handleShareDialog(false)}
          >
            <EmailIcon className={classes.shareButtonIcon} />
            <span className={classes.shareButtonText}>Enviar por Mail</span>
          </Button>
        </DialogContent>
      </Dialog>
    )
  }
}

Share.propTypes = {
  classes: PropTypes.object.isRequired,
  shareDialog: PropTypes.bool.isRequired,
  handleShareDialog: PropTypes.func.isRequired,
};

export default withStyles(styles)(Share);
