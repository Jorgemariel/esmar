import React from 'react';
import PropTypes from 'prop-types';

// Icons
import { FaBed, FaBath, FaRegCompass } from 'react-icons/fa';
import CheckIcon from '@material-ui/icons/Check';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import GarageIcon from 'components/ui/garageIcon';

// Material
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from 'components/ui/material/paper';

// Custom
import Feature from 'components/propiedad/feature';

const styles = {
  root: {
    padding: '15px 20px',
    width: '100%',
  },
  wrapper: {
    maxWidth: 400,
    margin: '0 auto',
  },
  feature: {
    padding: '15px 10px',

    '&:nth-child(even)': {
      background: 'var(--lightgrey-color)',
    }
  },
};

const Features = ({ features, classes }) => (
  <div className={classes.root}>
    <Typography variant="h6" gutterBottom>Caracteristicas</Typography>
    <Paper elevation={1} className={classes.wrapper}>
      {!!features.garage &&
        <div className={classes.feature}><Feature name='Cochera' icon={<GarageIcon />} text={<CheckIcon />} /></div>
      }
      {!!features.bathrooms &&
        <div className={classes.feature}><Feature name='Banos' icon={<FaBath />} text={features.bathrooms} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Dormitorios' icon={<FaBed />} text={features.bedrooms} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Antiguedad (anos)' icon={<AccessTimeIcon />} text={'20'} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Orientacion' icon={<FaRegCompass />} text={'N'} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Disposicion' icon={<FaBed />} text={'Lateral'} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Superficie Total' icon={<FaBed />} text={<span>25 m<sup>2</sup></span>} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Superficie Cubierta' icon={<FaBed />} text={<span>20 m<sup>2</sup></span>} /></div>
      }
      {!!features.bedrooms &&
        <div className={classes.feature}><Feature name='Ambientes' icon={<FaBed />} text={'5'} /></div>
      }
    </Paper>
  </div>
);

Features.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Features)