import React from 'react';
import PropTypes from 'prop-types';

// Material
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    padding: '15px 20px',
    textAlign: 'justify',
  },
};

const Description = ({ text, classes }) => (
  <div className={classes.root}>
    <Typography variant="h6" gutterBottom>Descripcion</Typography>
    <Typography variant="body2" gutterBottom>{text}</Typography>
  </div>
);

Description.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Description)