import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import getConfig from 'next/config';

// Icons
import { FaWhatsapp, FaRegCalendarAlt } from 'react-icons/fa';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';


//Material
import { withStyles } from '@material-ui/core/styles';
import Button from 'components/ui/material/button';
import Dialog from 'components/ui/material/dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { Typography, TextField } from '@material-ui/core';

const styles = {
  dialog: {
    margin: 10,
  },
  dialogContent: {
    padding: '0 !important',
  },
  contactButton: {
    margin: 20,
    padding: '20px 15px',
    width: '-webkit-fill-available',
    //width: '-moz-available',
    display: 'flex',
  },
  contactButtonIcon: {
    width: 30,
    fontSize: 'x-large',
  },
  contactButtonText: {
    flex: 1,
    paddingLeft: 15,
    textAlign: 'left',
  },
  whatsapp: {
    backgroundColor: '#65B256',
    color: 'white',
  },
  calendar: {
    backgroundColor: '#1383D8',
    color: 'white',
  },
  email: {
    backgroundColor: '#E04F4C',
    color: 'white',
  },
  call: {
    backgroundColor: '#30B943',
    color: 'white',
  },
  form: {
    padding: 20,
    textAling: 'center',
  },
  formTitle: {
  },
  formField: {
    display: 'flex',
    width: '100%',
  },
  formButton: {
    display: 'block',
    margin: '0 auto',
    marginTop: 20,
  }
};

const forms = Object.freeze({
  EVENT: 'EVENT',
  CALL_ME: 'CALL_ME',
});

const { publicRuntimeConfig: { COMPANY_EMAIL } } = getConfig();

class Contact extends React.Component {
  state = {
    showForm: false,
    form: {},
    emailSubject: encodeURIComponent('Consulta desde pagina web'),
    companyPhone: 3514800200,
    companyPhoneToString: '(351) 4 800 200',
    whatsappPhone: 5493515144316,
  }

  componentDidMount() {
    if (typeof document !== 'undefined') {
      this.setState({
        message: encodeURIComponent(`Hola, \nMe encuentro interesado en la propiedad publicada en ${document.location.href}`),
      });
    }
  }

  onChangeFormField = (key, value) => {
    this.setState((state) => ({
      form: {
        ...state.form,
        [key]: value,
      },
    }));
  }

  closeDialog = () => {
    this.props.handleContactDialog(false);

    //Wait the animation
    setTimeout(() => this.setState({ showForm: false }), 300);

  }

  openForm = form => {
    this.setState({ showForm: forms[form] });
  }

  render() {
    const { contactDialog, classes } = this.props;
    const { showForm, form, message, whatsappPhone, emailSubject, companyPhone, companyPhoneToString } = this.state;

    return (
      <Dialog
        open={contactDialog}
        onClose={this.closeDialog}
        PaperProps={{ classes: { root: classes.dialog } }}
      >
        <DialogContent className={classes.dialogContent}>
          {!showForm &&
            <div>
              <Button
                size="large"
                className={classNames(classes.contactButton, classes.calendar)}
                onClick={() => this.openForm(forms.EVENT)}
              >
                <FaRegCalendarAlt className={classes.contactButtonIcon} />
                <span className={classes.contactButtonText}>Agendar Visita</span>
              </Button>
              <Button
                size="large"
                className={classNames(classes.contactButton, classes.call)}
                href={`tel:${companyPhone}`}
                onClick={this.closeDialog}
              >
                <PhoneIcon className={classes.contactButtonIcon} />
                <span className={classes.contactButtonText}>{companyPhoneToString}</span>
              </Button>
              <Button
                size="large"
                className={classNames(classes.contactButton, classes.whatsapp)}
                href={`https://wa.me/${whatsappPhone}?text=${message}`}
                onClick={this.closeDialog}
              >
                <FaWhatsapp className={classes.contactButtonIcon} />
                <span className={classes.contactButtonText}>Enviar Whatsapp</span>
              </Button>
              <Button
                size="large"
                className={classNames(classes.contactButton, classes.email)}
                href={`mailto:${COMPANY_EMAIL}?subject=${emailSubject}&body=${message}`}
                onClick={this.closeDialog}
              >
                <EmailIcon className={classes.contactButtonIcon} />
                <span className={classes.contactButtonText}>Enviar Mail</span>
              </Button>
              <Button
                size="large"
                className={classNames(classes.contactButton, classes.call)}
                onClick={() => this.openForm(forms.CALL_ME)}
              >
                <PhoneIcon className={classes.contactButtonIcon} />
                <span className={classes.contactButtonText}>Quiero que me llamen</span>
              </Button>
            </div>
          }
          {showForm === forms.EVENT &&
            <div className={classes.form}>
              <Typography variant="h6" gutterBottom className={classes.formTitle}>Agendar Visita</Typography>
              <TextField
                label="Nombre y apellido"
                value={form.name}
                onChange={(e) => this.onChangeFormField('name', e.target.value)}
                className={classes.formField}
                type="text"
                name="name"
                autoComplete="name"
                margin="normal"
                variant="outlined"
              />
              <TextField
                label="Telefono"
                value={form.phone}
                onChange={(e) => this.onChangeFormField('phone', e.target.value)}
                className={classes.formField}
                type="tel"
                name="phone"
                autoComplete="tel"
                margin="normal"
                variant="outlined"
              />
              <TextField
                value={form.calendar}
                onChange={(e) => this.onChangeFormField('calendar', e.target.value)}
                label="¿En que horario te gustaria visitar la propiedad?"
                multiline
                rows="4"
                className={classes.formField}
                margin="normal"
                variant="outlined"
              />

              <Button size="large" className={classNames(classes.formButton, classes.calendar)}>Solicitar</Button>
            </div>
          }
          {showForm === forms.CALL_ME &&
            <div className={classes.form}>
              <Typography variant="h6" gutterBottom className={classes.formTitle}>Quiero que me llamen</Typography>
              <TextField
                label="Nombre y apellido"
                value={form.name}
                onChange={(e) => this.onChangeFormField('name', e.target.value)}
                className={classes.formField}
                type="text"
                name="name"
                autoComplete="name"
                margin="normal"
                variant="outlined"
              />
              <TextField
                label="Telefono"
                value={form.phone}
                onChange={(e) => this.onChangeFormField('phone', e.target.value)}
                className={classes.formField}
                type="text"
                name="phone"
                autoComplete="phone"
                margin="normal"
                variant="outlined"
              />
              <TextField
                value={form.callme}
                onChange={(e) => this.onChangeFormField('callme', e.target.value)}
                label="¿En que horario podemos contactarte?"
                multiline
                rows="4"
                className={classes.formField}
                margin="normal"
                variant="outlined"
              />
              <Button size="large" className={classNames(classes.formButton, classes.call)}>Solicitar</Button>
            </div>
          }
        </DialogContent>
      </Dialog>
    )
  }
}

Contact.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Contact);
