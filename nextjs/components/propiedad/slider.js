import Swiper from 'react-id-swiper/lib/custom';
import PropTypes from 'prop-types';
import Lightbox from 'react-image-lightbox';
import { withStyles } from '@material-ui/core/styles';

import 'react-image-lightbox/style.css';

const styles = {
  name: {
    position: 'absolute',
    color: 'white',
    left: 0,
    right: 0,
    textAlign: 'center',
    zIndex: 99,
    paddingTop: 15,
    background: 'rgba(0, 0, 0, 0.5)',
    boxShadow: '0px 20px 20px 0px rgba(0,0,0,0.5)',
    '-webkit-appearance': 'none',
    '-webkit-box-shadow': '0px 20px 20px 0px rgba(0,0,0,0.5)',
    '-moz-box-shadow': '0px 20px 20px 0px rgba(0,0,0,0.5)',
    //background: 'linear-gradient(to bottom, black, rgba(255, 255, 255, 0))',
  },
  swiperWrapper: {
    maxHeight: 300,
  },
  swiperSlide: {
    maxHeight: 300,
  },
  imageWrapper: {
    height: '100%',
    width: '100%',
  },
  image: {
    maxHeight: '100%',
    maxWidth: '100%',
  }
};

class Slider extends React.Component {
  state = {
    photoIndex: 0,
    viewer: false,
  }

  goNext = () => {
    const { images } = this.props;
    const { photoIndex } = this.state;

    this.swiper.slideNext();
    this.setState({
      photoIndex: (photoIndex + 1) % images.length,
    })
  }

  goPrev = () => {
    const { images } = this.props;
    const { photoIndex } = this.state;

    this.swiper.slidePrev();
    this.setState({
      photoIndex: (photoIndex + images.length - 1) % images.length,
    })
  }

  toggleViewer = (photoIndex, viewer) => {
    this.setState({ photoIndex, viewer })
  }

  render() {
    const { images, name, classes } = this.props;
    const { viewer, photoIndex } = this.state;

    const params = {
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      },
    }

    return (
      <div>
        <div className={classes.name}>
          <span>{name}</span>
        </div>
        <Swiper
          // wrapperClass={classes.swiperWrapper}
          // slideClass={classes.swiperSlide}
          ref={(node) => { if (node) this.swiper = node.swiper }}
          {...params}
        >
          {
            images.map((image, i) => (
              <img key={i} onClick={() => this.toggleViewer(i, true)} src={image} alt="" />
            ))
          }
        </Swiper>

        {viewer &&
          <Lightbox
            mainSrc={images[photoIndex]}
            nextSrc={images[(photoIndex + 1) % images.length]}
            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
            onCloseRequest={() => this.toggleViewer(0, false)}
            onMovePrevRequest={() => this.goPrev()}
            onMoveNextRequest={() => this.goNext()}
            imagePadding={0}
            imageTitle={name}
          />
        }
      </div>
    )
  }
}

Slider.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Slider);