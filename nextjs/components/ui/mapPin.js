import React from 'react';
import PropTypes from 'prop-types';
import { FaMapMarkerAlt } from 'react-icons/fa';

// Material
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    fontSize: '3em',
    color: 'var(--primary-color)',
    position: 'absolute',
    transform: 'translate(-50%, -50%)',
  },
};

const Pin = ({ classes }) => (
  <div className={classes.root}>
    <FaMapMarkerAlt />
  </div>
);

Pin.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Pin)