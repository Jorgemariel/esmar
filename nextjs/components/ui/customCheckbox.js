import '../../static/css/customCheckbox.css';

const CustomCheckbox = ({ label, name, checked, onChange }) => (
  <div>
    <label className="container">
      <input
        type='checkbox'
        name={name}
        checked={checked}
        onChange={onChange}
      />
      <span className="checkmark" />
      <span className='label'>{label}</span>
    </label>
  </div>
)

export default CustomCheckbox;