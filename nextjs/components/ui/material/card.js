import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MaterialCard from '@material-ui/core/Card';

// The `withStyles()` higher-order component is injecting a `classes`
// property that is used by the `Card` component.
const Card = withStyles({
  root: {
    borderRadius: 'var(--border-radius)',
  },
})(MaterialCard);

export default Card;