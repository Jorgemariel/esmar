import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MaterialChip from '@material-ui/core/Chip';

// The `withStyles()` higher-order component is injecting a `classes`
// property that is used by the `Chip` component.
const Chip = withStyles({
  root: {
    borderRadius: 'var(--border-radius)',
  },
})(MaterialChip);

export default Chip;