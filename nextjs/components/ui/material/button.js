import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MaterialButton from '@material-ui/core/Button';

// The `withStyles()` higher-order component is injecting a `classes`
// property that is used by the `Button` component.
const Button = withStyles({
  root: {
    borderRadius: 'var(--border-radius)',
  },
})(MaterialButton);

export default Button;