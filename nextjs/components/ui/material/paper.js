import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MaterialPaper from '@material-ui/core/Paper';

// The `withStyles()` higher-order component is injecting a `classes`
// property that is used by the `Paper` component.
const Paper = withStyles({
  root: {
    borderRadius: 'var(--border-radius)',
  },
})(MaterialPaper);

export default Paper;