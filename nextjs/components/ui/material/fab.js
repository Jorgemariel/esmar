import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MaterialFab from '@material-ui/core/Fab';

// The `withStyles()` higher-order component is injecting a `classes`
// property that is used by the `Fab` component.
const Fab = withStyles({
  root: {
    borderRadius: 'var(--border-radius)',
  },
})(MaterialFab);

export default Fab;