import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import MaterialDialog from '@material-ui/core/Dialog';

// The `withStyles()` higher-order component is injecting a `classes`
// property that is used by the `Dialog` component.
const Dialog = withStyles({
  paper: {
    borderRadius: 'var(--border-radius)',
  },
})(MaterialDialog);

export default Dialog;