import { getJSON, setJSON } from 'helpers/localStoraje';
import _ from 'lodash';

export const getFavs = () => {
  return getJSON('favs');
}

export const isFav = id => {
  const favs = getJSON('favs');
  return favs.includes(id);
}

export const addFav = id => {
  // @TODO: log
  const favs = [
    ...getFavs(),
    id
  ]
  setJSON('favs', favs);
}

export const removeFav = id => {
  // @TODO: log
  const favs = getFavs();
  const idx = favs.indexOf(id);

  if (idx == -1) {
    return new Error('The property was not fav');
  }
  favs.splice(idx, 1);
  setJSON('favs', favs);
}