const routes = require('next-routes')

module.exports = routes()
  .add('propiedad', '/propiedad/:name/:id', 'propiedad')
  .add('search', '/buscar/:filter', 'propiedades')