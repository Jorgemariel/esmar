import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Link } from '../routes';

// Material
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

// Esmar
import withLayout from 'layouts/main';
import Options from 'components/search/options';
import AppliedFilters from 'components/search/appliedFilters';
import PropertyStandardView from 'components/search/propertyStandardView';
import PropertyPinView from 'components/search/propertyPinView';
import OrderBy from 'components/search/orderBy';
import { filterResolver, urlGenerator } from 'helpers/filterResolver';
import { getPropertiesWithFilters, getNeighborhoods } from 'actions/properties';
import ModalFilter from 'components/search/modalFilter';
import Actions from 'components/search/actions';
import Map from 'components/search/map';
import { addFav, removeFav, getFavs } from 'actions/favs';
import { viewTypes } from 'helpers/defaults';

const styles = {
  root: {
    height: '100%',
    overflow: 'auto',
  },
  hr: {
    margin: 0,
    border: 0,
    borderTop: '1px solid #DDD',
  },
  numberOfResults: {
    textAlign: 'center',
    margin: '20px 10px 10px 10px',
  },
  viewWrapper: {
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
  },
  standardView: {
    maxWidth: '90%',
    minWidth: 350,
    margin: 10,
    position: 'relative',
  },
  actions: {
    position: 'absolute',
    top: 5,
    right: 5,
    zIndex: 99,
  },
  aTag: {
    color: 'inherit',
    textDecoration: 'inherit',
  }
};

class Propiedades extends React.Component {
  static async getInitialProps(context) {
    const {
      filter,
      dormitorios,
      precio_max,
      precio_min,
      cocheras,
      banos,
    } = context.query;

    // Get filters from URL
    let urlFilters = filter ? filterResolver(filter) : {
      types: [],
      operation: null,
      neighborhoods: [],
    };

    const neighborhoods = await getNeighborhoods({ context });

    if (urlFilters.neighborhoods) urlFilters = {
      ...urlFilters,
      neighborhoods: this.getNeighborhoodsIdsByNames(urlFilters.neighborhoods, neighborhoods),
    };

    const filters = {
      ...urlFilters,
      dormitorios,
      precio_max,
      precio_min,
      cocheras,
      banos,
    }

    const results = await getPropertiesWithFilters(filters, { context });

    return {
      filters,
      results,
      neighborhoods,
    };
  }

  constructor(props) {
    super(props);

    this.state = {
      neighborhoods: props.neighborhoods,
      filters: props.filters, //Applied filters
      results: props.results || [],
      showModalFilter: false,
      favs: [], //Local Favs
      selectedView: viewTypes.MAP, //viewTypes.LIST,

      selectedOrder: 'RELEVANTS',
      orderDialog: null,
      filterDialog: null,
      viewDialog: null,
      showSnackbar: false,
    };
  }

  componentDidMount() {
    this.updateFavs();
  }

  static getNeighborhoodsIdsByNames(names, neighborhoods) {
    const filteredNeighborhoods = _.filter(neighborhoods, nh => names.includes(nh.label));
    return filteredNeighborhoods.map(nh => nh.id);
  }

  handleModalFilter = (e, showModalFilter) => {
    this.setState({ showModalFilter });
  }

  onDeleteFilter = (item, key = null) => {
    const { filters } = this.state;

    if (key) {
      const index = filters[item].indexOf(key);

      if (index > -1) filters[item].splice(index, 1);
    } else {
      delete filters[item];
    }

    this.setState({ filters });
    this.updateUrl();
    this.reload();
  }

  onAddFilter = (key, value) => {
    const { filters } = this.state;

    if (['neighborhoods', 'types'].includes(key)) {
      if (!filters[key]) filters[key] = [];

      const idx = filters[key].indexOf(value);

      if (idx === -1) {
        filters[key].push(value);
      } else {
        filters[key].splice(idx, 1);
      }
    } else {
      filters[key] = value;
    }

    this.setState({ filters });
    this.updateUrl();
    this.reload();
  }

  updateUrl = () => {
    let { filters } = this.state;
    if (filters.neighborhoods) filters = {
      ...filters,
      neighborhoods: this.getNeighborhoodsNamesByIds(filters.neighborhoods),
    };

    const url = urlGenerator(filters);

    if (history) {
      history.pushState(null, '', url);
    }
  }

  getNeighborhoodsNamesByIds = ids => {
    const neighborhoods = _.filter(this.props.neighborhoods, nh => ids.includes(nh.id));
    return neighborhoods.map(nh => nh.label);
  }

  handleOrderModal = orderDialog => {
    this.setState({ orderDialog });
  }

  toggleView = () => {
    this.setState((state) => ({
      selectedView: state.selectedView === viewTypes.LIST ? viewTypes.MAP : viewTypes.LIST,
    }));
  }

  onChangeOrder = selectedOrder => {
    this.setState({ selectedOrder });
    this.handleOrderModal(null);
    this.updateOrder();
  }

  onFav = (e, id) => {
    let message;

    try {
      if (e.target.checked) {
        addFav(id);
        message = 'Agregado a favoritos';
      } else {
        removeFav(id);
        message = 'Eliminado de favoritos';
      }
    } catch (error) {
      console.log({ error });
    } finally {
      this.updateFavs();
      this.showSnackbar(message);
    }
  }

  updateFavs = () => {
    this.setState({ favs: getFavs() })
  }
  onShare = id => {
    // @TODO mostrar modal? compartir solo en whatsapp? realmente vale la pena?
    // @TODO log action
    console.log({ id })
  }

  showSnackbar = message => {
    this.setState({
      snackbarMessage: message,
      showSnackbar: true,
    });
  }

  hideSnackbar = (e, reason) => {
    if (reason === 'clickaway') return;
    this.setState({ showSnackbar: false });
  }

  getUrl = name => {
    return name.replace(/\W+/g, '-').toLowerCase();
  }

  async reload() {
    const results = await getPropertiesWithFilters(this.state.filters);
    this.setState({ results });
  }

  render() {
    const { classes, neighborhoods } = this.props;
    const {
      results,
      filters,
      showModalFilter,
      favs,
      selectedView,

      selectedOrder,
      orderDialog,
    } = this.state

    return (
      <div className={classes.root}>
        <Options
          openFilter={(e) => this.handleModalFilter(e, true)}
          selectedView={selectedView}
          toggleView={this.toggleView}

          openOrder={(e) => this.handleOrderModal(e.currentTarget)}
          openView={(e) => this.handleViewModal(e, true)}

          orderDialog={orderDialog}
        />
        <hr className={classes.hr} />
        <AppliedFilters items={filters} neighborhoodsList={neighborhoods} onDelete={this.onDeleteFilter.bind(this)} />
        <div className={classes.viewWrapper}>
          {selectedView === viewTypes.LIST ?
            <React.Fragment>
              <Typography variant="subtitle1" gutterBottom className={classes.numberOfResults}>
                {results.length} propiedades encontradas
              </Typography>
              {results.map(item =>
                <div key={item.id} className={classes.standardView}>
                  <Link route={`/propiedad/${this.getUrl(item.name)}/${item.id}`}>
                    <a className={classes.aTag}>
                      <PropertyStandardView
                        data={item} key={item.id}
                        onShare={this.onShare}
                      />
                    </a>
                  </Link>
                  <div className={classes.actions}>
                    <Actions fav={favs.includes(item.id)} onFav={(e) => this.onFav(e, item.id)} onShare={() => this.onShare(item.id)} />
                  </div>
                </div>
              )}
            </React.Fragment>
            :
            <React.Fragment>
              <Map>
                <PropertyPinView
                  lat={-31.4076867}
                  lng={-64.2125214}
                  text={'Pin'}
                />
                <PropertyPinView
                  lat={-31.41}
                  lng={-64.22}
                  text={'Pin'}
                />
                <PropertyPinView
                  lat={-31.42}
                  lng={-64.20}
                  text={'Pin'}
                />
              </Map>
            </React.Fragment>
          }
        </div>

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={this.state.showSnackbar}
          autoHideDuration={3000}
          onClose={this.hideSnackbar}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.hideSnackbar}
            >
              <CloseIcon />
            </IconButton>,
          ]}
          message={<span id="message-id">{this.state.snackbarMessage}</span>}
        />

        <ModalFilter
          open={showModalFilter}
          filters={filters}
          neighborhoods={neighborhoods}
          onClose={() => this.handleModalFilter(false)}
          onChange={this.onAddFilter.bind(this)}
          onDelete={this.onDeleteFilter.bind(this)}
        />

        <OrderBy
          orderDialog={orderDialog}
          onClose={() => this.handleOrderModal(null)}
          selectedOrder={selectedOrder}
          onChangeOrder={this.onChangeOrder.bind(this)}
        />
      </div>
    )
  }
}

Propiedades.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withLayout(withStyles(styles)(Propiedades));