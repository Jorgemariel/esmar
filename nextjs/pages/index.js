import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

//Material
import { withStyles } from '@material-ui/core/styles';

//Custom
import withLayout from 'layouts/main';
import Search from 'components/index/search';
import FloatingAction from 'components/index/floatingAction';
import { getNeighborhoods } from 'actions/properties';

const styles = {
  root: {
    backgroundImage: 'url(static/img/search-background-mobile.1.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    boxShadow: 'inset 0 0 0 1000px rgba(0,0,0,.5)',
    color: 'white',
    height: '100%',
    position: 'relative',
  },
  contentWrapper: {
    //height: '-webkit-fill-available',
    //display: 'flex',
    //alignItems: 'center',
    position: 'fixed',
    top: '35%',
    transform: 'translate(0, -35%)',
    left: 0,
    right: 0,
  },
  content: {
    margin: '0 auto',
    marginBottom: 50,
  },
  slogan: {
    textAlign: 'center',
    maxWidth: '80%',
    margin: '0 auto',
    marginBottom: 70,
  },
  floatingAction: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    left: 20,
  },
};

class Index extends React.Component {
  static async getInitialProps(context) {
    //console.log({ context })
    const areas = await getNeighborhoods({ context });

    return {
      areas,
    };
  }

  render() {
    const { areas, classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.contentWrapper}>
          <div className={classes.content}>
            <h1 className={classes.slogan}>Mucho más de lo que usted espera</h1>
            <Search areas={areas} />
          </div>
        </div>
        <div className={classNames(classes.floatingAction, 'hide-on-keyboard')}>
          <FloatingAction />
        </div>
      </div>
    );
  }
};

Index.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withLayout(withStyles(styles)(Index))
