import React from 'react';
import PropTypes from 'prop-types';
import { Router } from '../routes';

//Material
import { withStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

//Custom
import withLayout from 'layouts/propiedad';
import Header from 'components/propiedad/header';
import Slider from 'components/propiedad/slider';
import Price from 'components/propiedad/price';
import Description from 'components/propiedad/description';
import Features from 'components/propiedad/features';
import Services from 'components/propiedad/services';
import Amenities from 'components/propiedad/amenities';
import Map from 'components/propiedad/map';
import Actions from 'components/propiedad/actions';
import Contact from 'components/propiedad/contact';
import Share from 'components/propiedad/share';
import { isFav, addFav, removeFav } from 'actions/favs';

const styles = {
  root: {
    paddingBottom: 64,
  },
  marginTop20: {
    marginTop: 10,
  },
};

class Propiedad extends React.Component {
  static async getInitialProps(context) {
    //console.log({ context })
    const data = {
      id: 6789,
      name: 'HUDSON GUILLERMO 4500',
      price: 500000,
      coin: 'USD',
      neighborhood: 'ALTO ALBERDI',
      alt: 'Descripcion corta',
      description: 'Propiedad ubicada en B° Parque Liceo II secc., enfrente del nuevo dispensario municipal y a dos cuadras de Av. Storni. Casa en excelente estado de conservación general. Cuenta con dos dormitorios con placard, living/comedor, cocina equipada, baño, patio con quincho y cochera techada.',
      type: 'Departamento',
      address: 'HUDSON GUILLERMO 4558',
      stared: 0,
      operation: 'Venta',
      google_maps: 'https://www.google.com.ar/maps/place/Hip%C3%B3lito+Vieytes+78-98,+X5002JGB+C%C3%B3rdoba/@-31.4076867,-64.2125214,3a,66.8y,100.84h,85.61t/data=!3m4!1e1!3m2!1sYALXtnJR0HbucGoPiR2XRA!2e0!4m2!3m1!1s0x94329890aba7addf:0xbf1e257cc9250405',
      features: {
        bedrooms: 3,
        bathrooms: 2,
        garage: 1,
      },
      services: {
        light: true,
        water: true,
        gas: true,
      },
      amenities: {
        private_security: true,
        swimming_pool: true,
        gym: true,
      },
      images: [
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/009631/009631_0_0000021228.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/003629/003629_0_0000030186.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/009631/009631_0_0000021228.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/003629/003629_0_0000030186.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/009631/009631_0_0000021228.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/003629/003629_0_0000030186.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/009631/009631_0_0000021228.jpg',
        'http://www.esmar.com.ar/wp-content/uploads/propiedades/003629/003629_0_0000030186.jpg',
      ],
      meta: {
        keywords: ['departamento', 'nueva cordoba', 'venta'],
      },
    };

    return {
      data,
    };
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props.location)
    if (nextProps.location !== this.props.location) {
      this.setState({ prevPath: this.props.location })
    }
  }

  componentDidMount() {
    this.updateFavs();
  }

  state = {
    prevPath: null,
    contactDialog: false,
    shareDialog: false,
    fav: false,
    snackbarMessage: '',
    showSnackbar: false,
  };

  updateFavs = () => {
    const { data: { id } } = this.props;
    this.setState({ fav: isFav(id) })
  }

  onFav = (e, id) => {
    let message;

    try {
      if (e.target.checked) {
        addFav(id);
        message = 'Agregado a favoritos';
      } else {
        removeFav(id);
        message = 'Eliminado de favoritos';
      }
    } catch (error) {
      console.log({ error });
    } finally {
      this.updateFavs();
      this.showSnackbar(message);
    }
  }

  showSnackbar = message => {
    this.setState({
      snackbarMessage: message,
      showSnackbar: true,
    });
  }

  hideSnackbar = (e, reason) => {
    if (reason === 'clickaway') return;
    this.setState({ showSnackbar: false });
  }

  onShare = (e, id) => {
    console.log('onShare');
  }

  handleCopied = () => {
    this.showSnackbar('Enlace copiado en portapapeles.');
  }

  handleContactDialog = contactDialog => {
    this.setState({ contactDialog });
  }

  handleShareDialog = shareDialog => {
    this.setState({ shareDialog });
  }

  render() {
    const { data, classes } = this.props;
    const { fav, contactDialog, shareDialog } = this.state;

    return (
      <div className={classes.root}>
        <Header fav={fav} onFav={(e) => this.onFav(e, data.id)} onShare={() => this.handleShareDialog(true)} />
        <Slider images={data.images} name={data.name} />
        <Price price={data.price} coin={data.coin} type={data.type} operation={data.operation} />
        <Description text={data.description} />
        {Object.keys(data.features).length !== 0 &&
          <Features features={data.features} />
        }
        {Object.keys(data.services).length !== 0 &&
          <Services services={data.services} />
        }
        {Object.keys(data.amenities).length !== 0 &&
          <Amenities amenities={data.amenities} />
        }
        <div className={classes.marginTop20}>
          <Map google_maps={data.google_maps} />
        </div>

        <Actions onClickContact={this.handleContactDialog} />
        <Contact contactDialog={contactDialog} handleContactDialog={this.handleContactDialog} />
        <Share shareDialog={shareDialog} handleShareDialog={this.handleShareDialog} handleCopied={this.handleCopied} data={data} />

        <Snackbar
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
          open={this.state.showSnackbar}
          autoHideDuration={3000}
          onClose={this.hideSnackbar}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.hideSnackbar}
            >
              <CloseIcon />
            </IconButton>,
          ]}
          message={<span id="message-id">{this.state.snackbarMessage}</span>}
        />
      </div>
    );
  }
};

Propiedad.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withLayout(withStyles(styles)(Propiedad));